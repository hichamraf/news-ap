package hicham.com.bonialnews.home

import com.google.gson.Gson
import hicham.com.bonialnews.data.ArticleRepository
import hicham.com.bonialnews.model.Article
import hicham.com.bonialnews.model.NewsApiResponse
import hicham.com.bonialnews.ui.home.MainViewModel
import hicham.com.bonialnews.ui.home.MainViewModel.RequestState.COMPLETE
import io.reactivex.Single
import io.reactivex.SingleEmitter
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class MainViewModelTest {

    private lateinit var mainViewModel: MainViewModel
    private lateinit var mockArticlesList: List<Article>
    private var articleRepository = Mockito.mock(ArticleRepository::class.java)
    private lateinit var articleObservable: Single<List<Article>>
    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Before
    fun initTest() {
        mockArticlesList =
            (Gson().fromJson<NewsApiResponse>(mockArticlesJson, NewsApiResponse::class.java)).articles.toList()

    }

    @Test
    fun `test that on start app vm gets no more than 21 articles`() {
        articleObservable = Single.create { e: SingleEmitter<List<Article>> -> e.onSuccess(mockArticlesList.take(21)) }
        Mockito.`when`(articleRepository.getArticles(Mockito.anyInt())).thenReturn(articleObservable)
        mainViewModel = MainViewModel(articleRepository)
        Assert.assertEquals(21, mainViewModel.articlesObservale.value.size)
        Assert.assertFalse(mainViewModel.lastArticlLoaded)
        Assert.assertEquals(0, mainViewModel.lastRequestedPage)

    }

    @Test
    fun `test last articles are loaded is detected`() {
        articleObservable = Single.create { e: SingleEmitter<List<Article>> -> e.onSuccess(mockArticlesList.take(17)) }
        Mockito.`when`(articleRepository.getArticles(Mockito.anyInt())).thenReturn(articleObservable)
        mainViewModel = MainViewModel(articleRepository)
        Assert.assertEquals(true, mainViewModel.lastArticlLoaded)
        Assert.assertEquals(COMPLETE, mainViewModel.requestsObservable.value)
    }

    // this line may cause a compilation error, the problem is caused by corrupt incremental compilation caches; a clean rebuild will solve it/ invalidate cache and restart
    var mockArticlesJson = "{\n" +
            "    \"status\": \"ok\",\n" +
            "    \"totalResults\": 38,\n" +
            "    \"articles\": [\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"fox-news\",\n" +
            "                \"name\": \"Fox News\"\n" +
            "            },\n" +
            "            \"author\": \"Alexandra Deabler\",\n" +
            "            \"title\": \"Chick-fil-A's mac and cheese hits menus nationwide - Fox News\",\n" +
            "            \"description\": \"Chick-fil-A’s mac and cheese is officially on the menu nationwide.\",\n" +
            "            \"url\": \"https://www.foxnews.com/food-drink/chick-fil-mac-cheese-nationwide\",\n" +
            "            \"urlToImage\": \"https://static.foxnews.com/foxnews.com/content/uploads/2019/08/chick-fil-a-mac-and-cheese.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T16:18:55Z\",\n" +
            "            \"content\": \"Chick-fil-A’s mac and cheese is officially on the menu nationwide.\\r\\nThe fan-favorite chicken chain announced mac and cheese has been added as a side option with any lunch, dinner, Kid’s Meal or catering order at participating locations across the country.\\r\\nPO… [+1884 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Npr.org\"\n" +
            "            },\n" +
            "            \"author\": \"Clare Lombardo\",\n" +
            "            \"title\": \"Simone Biles Lands Historic Triple-Double, Earns 6th U.S. All-Around Gymnastics Title - NPR\",\n" +
            "            \"description\": \"The reigning world champion is the first woman to stick the landing after two flips and three full twists. Biles also made history by performing a double-double dismount off the balance beam Friday.\",\n" +
            "            \"url\": \"https://www.npr.org/2019/08/12/750434716/simone-biles-lands-historic-triple-double-earns-6th-u-s-all-around-gymnastics-ti\",\n" +
            "            \"urlToImage\": \"https://media.npr.org/assets/img/2019/08/12/ap_19224074136964_wide-4639248e4894d84dbd0f39c00414968252911892.jpg?s=1400\",\n" +
            "            \"publishedAt\": \"2019-08-12T16:00:00Z\",\n" +
            "            \"content\": \"Simone Biles competes on the balance beam at the U.S. Gymnastics Championships on Sunday.\\r\\nCharlie Riedel/AP\\r\\nThe first time that Simone Biles performed a triple-double at the U.S. Gymnastics Championships in Kansas City, she wasn't pleased. After soaring thr… [+956 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"cnn\",\n" +
            "                \"name\": \"CNN\"\n" +
            "            },\n" +
            "            \"author\": \"Marianne Garvey, CNN\",\n" +
            "            \"title\": \"John Oliver tackles Trump's 'lack of humanity' in response to recent mass shootings - CNN\",\n" +
            "            \"description\": \"John Oliver was not impressed by President Donald Trump's response to the recent mass shootings.\",\n" +
            "            \"url\": \"https://www.cnn.com/2019/08/12/entertainment/john-oliver-trump-trnd/index.html\",\n" +
            "            \"urlToImage\": \"https://cdn.cnn.com/cnnnext/dam/assets/190609232907-john-oliver-last-week-tonight-file-super-tease.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T15:58:00Z\",\n" +
            "            \"content\": null\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"cnn\",\n" +
            "                \"name\": \"CNN\"\n" +
            "            },\n" +
            "            \"author\": \"David Shortell, CNN\",\n" +
            "            \"title\": \"Barr cites 'failure' at NYC jail that held Epstein, says 'co-conspirators' should not rest easy - CNN\",\n" +
            "            \"description\": \"Attorney General William Barr said investigators are learning of \\\"serious irregularities\\\" at the New York jail where accused sex trafficker Jeffrey Epstein was found dead of an apparent suicide over the weekend.\",\n" +
            "            \"url\": \"https://www.cnn.com/2019/08/12/politics/barr-new-york-jail-epstein/index.html\",\n" +
            "            \"urlToImage\": \"https://cdn.cnn.com/cnnnext/dam/assets/190723165739-william-bar-justice-deptartment-super-tease.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T15:23:00Z\",\n" +
            "            \"content\": \"New Orleans (CNN)Attorney General William Barr said investigators are learning of \\\"serious irregularities\\\" at the New York jail where accused sex trafficker Jeffrey Epstein was found dead of an apparent suicide over the weekend. \\r\\n\\\"We are now learning of seri… [+955 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"the-new-york-times\",\n" +
            "                \"name\": \"The New York Times\"\n" +
            "            },\n" +
            "            \"author\": null,\n" +
            "            \"title\": \"Trump Administration Weakens Protections for Endangered Species - The New York Times\",\n" +
            "            \"description\": \"The Trump administration announced far-reaching revisions to the Endangered Species Act, which was first enacted in 1973.\",\n" +
            "            \"url\": \"https://www.nytimes.com/2019/08/12/climate/endangered-species-act-changes.html\",\n" +
            "            \"urlToImage\": \"https://static01.nyt.com/images/2019/05/31/climate/00CLI-ENDANGERED-promo/00CLI-ENDANGERED-eagle-facebookJumbo.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T15:08:30Z\",\n" +
            "            \"content\": \"The rules also make it easier to remove a species from the endangered species list and weaken protections for threatened species a designation that means they are at risk of becoming endangered. It also gives the government new discretion in deciding what is … [+1021 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Phys.org\"\n" +
            "            },\n" +
            "            \"author\": \"Science X staff\",\n" +
            "            \"title\": \"Glitch in neutron star reveals its hidden secrets - Phys.org\",\n" +
            "            \"description\": \"Neutron stars are not only the most dense objects in the Universe, but they rotate very fast and regularly. Until they don't.\",\n" +
            "            \"url\": \"https://phys.org/news/2019-08-glitch-neutron-star-reveals-hidden.html\",\n" +
            "            \"urlToImage\": \"https://3c1703fe8d.site.internapcdn.net/newman/gfx/news/hires/2019/1-stars.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T15:00:19Z\",\n" +
            "            \"content\": \"Neutron stars are not only the most dense objects in the Universe, but they rotate very fast and regularly. Until they don't.\\r\\nOccasionally these neutron stars start to spin faster, caused by portions of the inside of the star moving outwards. It's called a \\\"… [+3165 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"fox-news\",\n" +
            "                \"name\": \"Fox News\"\n" +
            "            },\n" +
            "            \"author\": \"Madeline Farber\",\n" +
            "            \"title\": \"Legionnaires' disease outbreak at Atlanta hotel is now largest ever recorded in Georgia, health official says - Fox News\",\n" +
            "            \"description\": \"The outbreak of Legionnaires’ disease linked to an Atlanta hotel is now considered to be the largest ever recorded in Georgia, a state health official told Fox News.\",\n" +
            "            \"url\": \"https://www.foxnews.com/health/legionnaires-disease-outbreak-atlanta-largest\",\n" +
            "            \"urlToImage\": \"https://media2.foxnews.com/BrightCove/694940094001/2019/03/19/694940094001_6015706540001_6015709183001-vs.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T14:55:18Z\",\n" +
            "            \"content\": \"An outbreak of Legionnaires’ disease linked to an Atlanta hotel is now considered to be the largest ever recorded in Georgia, a state health official told Fox News.\\r\\nNancy Nydam, director of communications at the Georgia Department of Public Health, said Mond… [+2526 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Deadline.com\"\n" +
            "            },\n" +
            "            \"author\": \"Dade Hayes, Dade Hayes\",\n" +
            "            \"title\": \"CBS Stock Drifts Down, Viacom Sinks As Merger Deal Nears Close – Update - Deadline\",\n" +
            "            \"description\": \"UPDATED with stock movement. Shares of CBS slipped 1% after gaining in pre-market trading and Viacom’s pulled back more than 4% Monday trading as investors reacted to word that the companies …\",\n" +
            "            \"url\": \"https://deadline.com/2019/08/cbs-stock-jumps-at-opening-bell-viacom-sinks-as-merger-deal-nears-close-1202666225/\",\n" +
            "            \"urlToImage\": \"https://pmcdeadline2.files.wordpress.com/2018/05/cbs-viacom.jpg?w=630\",\n" +
            "            \"publishedAt\": \"2019-08-12T14:36:00Z\",\n" +
            "            \"content\": \"UPDATED with stock movement. Shares of CBS slipped 1% after gaining in pre-market trading and Viacom’s pulled back more than 4% Monday trading as investors reacted to word that the companies were near the finish line in their merger negotiations.\\r\\nThe boards … [+3227 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Wdrb.com\"\n" +
            "            },\n" +
            "            \"author\": \"\",\n" +
            "            \"title\": \"Health officials say mosquitoes with West Nile virus found in Louisville - WDRB\",\n" +
            "            \"description\": \"Officials say no human cases of the West Nile virus have been reported in Louisville so far this year.\",\n" +
            "            \"url\": \"https://www.wdrb.com/news/health-officials-say-mosquitoes-with-west-nile-virus-found-in/article_fb92fce6-bd08-11e9-b444-6b05e3a10fc3.html\",\n" +
            "            \"urlToImage\": \"https://bloximages.newyork1.vip.townnews.com/wdrb.com/content/tncms/assets/v3/editorial/5/39/539fc5b5-e86e-51d6-84e7-b974027ab760/5c05a745d5b8d.image.jpg?resize=640%2C360\",\n" +
            "            \"publishedAt\": \"2019-08-12T14:25:00Z\",\n" +
            "            \"content\": \"LOUISVILLE, Ky. (WDRB) -- The Department of Public Health and Wellness says mosquitoes infected with the West Nile virus have been found in one Louisville zip code.\\r\\nAccording to a news release, the infected mosquitoes were found in a trap in the Louisville z… [+1596 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"cnn\",\n" +
            "                \"name\": \"CNN\"\n" +
            "            },\n" +
            "            \"author\": \"Lisa Respers France, CNN\",\n" +
            "            \"title\": \"'The Crown' Season 3 premiere date announced - CNN\",\n" +
            "            \"description\": \"Her Majesty returns on November 17.\",\n" +
            "            \"url\": \"https://www.cnn.com/2019/08/12/entertainment/the-crown-season-3/index.html\",\n" +
            "            \"urlToImage\": \"https://cdn.cnn.com/cnnnext/dam/assets/190812150139-the-crown-season-3-super-tease.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T14:18:00Z\",\n" +
            "            \"content\": null\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"the-washington-post\",\n" +
            "                \"name\": \"The Washington Post\"\n" +
            "            },\n" +
            "            \"author\": \"Kayla Epstein\",\n" +
            "            \"title\": \"Fortnite star Ninja left Twitch. Then the platform recommended porn to his followers. - The Washington Post\",\n" +
            "            \"description\": \"Tyler Blevins, a.k.a \\\"Ninja,\\\" alleged Twitch mishandled his dormant account after he moved to Microsoft's Mixer.\",\n" +
            "            \"url\": \"https://www.washingtonpost.com/technology/2019/08/12/fortnite-star-ninja-left-twitch-then-platform-recommended-porn-his-followers/\",\n" +
            "            \"urlToImage\": \"https://www.washingtonpost.com/resizer/WmnxjgQ4BcDI47CBSvS8SadHYGo=/1484x0/arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/LX4JNJVUT4I6TLGIDWCHXLGKOM.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T14:06:57Z\",\n" +
            "            \"content\": \"Video game star Tyler Blevins, known to his fans as Ninja, had amassed over 14 million followers on the streaming platform Twitch, thanks to his mastery of the popular game Fortnite. But after he departed the platform for a rival streaming service, viewers wh… [+3605 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"the-new-york-times\",\n" +
            "                \"name\": \"The New York Times\"\n" +
            "            },\n" +
            "            \"author\": null,\n" +
            "            \"title\": \"Trump Rule Targets Legal Immigrants Who Rely on Food Stamps and Other Aid - The New York Times\",\n" +
            "            \"description\": \"A new rule is part of a sweeping policy aimed to narrow the demographic of people who come to live and work in the United States.\",\n" +
            "            \"url\": \"https://www.nytimes.com/2019/08/12/us/politics/trump-immigration-policy.html\",\n" +
            "            \"urlToImage\": \"https://static01.nyt.com/images/2019/08/12/us/politics/12dc-immig-promo/12dc-immig-promo-facebookJumbo-v2.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T14:03:45Z\",\n" +
            "            \"content\": \"Immigration advocates have pledged to sue the administration in an attempt to block the new regulation from going into effect. Tens of thousands of people opposed the rule in a public comment period over the past several months.\\r\\nThe regulation, also known as… [+886 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Deadline.com\"\n" +
            "            },\n" +
            "            \"author\": \"Dino-Ray Ramos, Dino-Ray Ramos\",\n" +
            "            \"title\": \"'The Morning Show' Trailer Gives First Look At Jennifer Aniston, Reese Witherspoon and Steve Carell In Apple TV+ Drama - Deadline\",\n" +
            "            \"description\": \"Apple TV+ has debuted its first teaser trailer for their forthcoming drama The Morning Show starring Academy Award-winner Reese Witherspoon, Emmy Award-winner Jennifer Aniston and Emmy Award-winner…\",\n" +
            "            \"url\": \"https://deadline.com/video/the-morning-show-apple-tv-plus-jennifer-aniston-reese-witherspoon-steve-carell/\",\n" +
            "            \"urlToImage\": \"https://pmcdeadline2.files.wordpress.com/2019/08/morning-show-e1565566037199.jpg?w=1000\",\n" +
            "            \"publishedAt\": \"2019-08-12T14:00:00Z\",\n" +
            "            \"content\": \"Apple TV+ has debuted its first teaser trailer for their forthcoming drama The Morning Show starring Academy Award-winner Reese Witherspoon, Emmy Award-winner Jennifer Aniston and Emmy Award-winner Steve Carell.\\r\\nWritten by Kerry Ehrin based on an original id… [+785 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Lifehacker.com\"\n" +
            "            },\n" +
            "            \"author\": \"Meghan Moravcik Walbert\",\n" +
            "            \"title\": \"The Perseid Meteor Shower Peaks Tonight - Lifehacker\",\n" +
            "            \"description\": \"The Perseids are upon us! Known as the most-anticipated meteor shower of the summer—boasting upwards of 100 meteors per hour—tonight is the night to catch the Perseid Meteor Shower’s peak action.\",\n" +
            "            \"url\": \"https://lifehacker.com/the-perseid-meteor-shower-peaks-tonight-1837099204\",\n" +
            "            \"urlToImage\": \"https://i.kinja-img.com/gawker-media/image/upload/s--hKBek82d--/c_fill,fl_progressive,g_center,h_900,q_80,w_1600/nw0vfk6vfcnuai3h8y9e.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T14:00:00Z\",\n" +
            "            \"content\": \"The Perseids are upon us! Known as the most-anticipated meteor shower of the summerboasting upwards of 100 meteors per hourtonight is the night to catch the Perseid Meteor Showers peak action.\\r\\nThe Perseids have been showering since July 13 and will continue … [+1527 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Thedailybeast.com\"\n" +
            "            },\n" +
            "            \"author\": \"Dean Obeidallah\",\n" +
            "            \"title\": \"Trump's (Almost) All-White GOP Emerges - Daily Beast\",\n" +
            "            \"description\": \"We know the president prefers white people. But a look at the percentages on his appointments gives new meaning to the phrase “White House.”\",\n" +
            "            \"url\": \"https://www.thedailybeast.com/trumps-almost-all-white-gop-emerges\",\n" +
            "            \"urlToImage\": \"https://img.thedailybeast.com/image/upload/c_crop,d_placeholder_euli9k,h_1687,w_3000,x_0,y_0/dpr_2.0/c_limit,w_740/fl_lossy,q_auto/v1565573351/190802-Dean-O-trump-tease_uqit3t\",\n" +
            "            \"publishedAt\": \"2019-08-12T13:49:00Z\",\n" +
            "            \"content\": \"After the 2014 midterm election, there was a sense the GOP was becoming a more racially diverse party when African-American Tim Scott won a seat to the U.S. Senate from South Carolina and Will Hurd and Mia Love were both elected to the House. Even NPR noted t… [+831 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Radio.com\"\n" +
            "            },\n" +
            "            \"author\": \"https://facebook.com/WEEI\",\n" +
            "            \"title\": \"Tom Brady on his new helmet via GHS: 'I don't really love the one that I'm in' - WEEI\",\n" +
            "            \"description\": \"When Tom Brady received his new helmet for his 20th season with the Patriots, one thing became clear: he doesn’t like it.\",\n" +
            "            \"url\": \"https://weei.radio.com/blogs/gabby-guerard/patriots-tom-brady-new-helmet-i-don-t-really-love-one-i-m\",\n" +
            "            \"urlToImage\": \"https://radioimg.s3.amazonaws.com/weei/s3fs-public/styles/nts_image_tall_hero_1170x415/public/USATS-%20Tom%20Brady.jpg?nca1WY9yEHvKAmCponFXshtMHZDHWmou&itok=U3NLrMGZ\",\n" +
            "            \"publishedAt\": \"2019-08-12T13:48:39Z\",\n" +
            "            \"content\": \"When Tom Brady received his new helmet for his 20th season with the Patriots, one thing became clear: he doesnt like it.\\r\\nListen to your team news NOW.Ive been experimenting with a couple different ones, and I dont really love the one that Im in, but I dont r… [+988 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Npr.org\"\n" +
            "            },\n" +
            "            \"author\": \"\",\n" +
            "            \"title\": \"Hong Kong Airport Shuts Down As Protesters Occupy It - NPR\",\n" +
            "            \"description\": \"Demonstrators filled the terminals, the latest in the 10th straight week of pro-democracy protests. China condemned the protests as \\\"signs of terrorism.\\\"\",\n" +
            "            \"url\": \"https://www.npr.org/2019/08/12/750404354/thousands-of-protesters-storm-hong-kong-airport-shutting-down-flights\",\n" +
            "            \"urlToImage\": \"https://media.npr.org/assets/img/2019/08/12/gettyimages-1161108283_wide-885579a51670263f5d854bbf7fee53acb8e52e63.jpg?s=1400\",\n" +
            "            \"publishedAt\": \"2019-08-12T13:43:00Z\",\n" +
            "            \"content\": \"Pro-democracy protesters occupy the departure hall of the Hong Kong International Airport, which was closed on Monday.\\r\\nAnthony Kwan/Getty Images\\r\\nUpdated at 10:11 a.m. ET\\r\\nThousands of demonstrators, wearing black clothing and carrying posters denouncing the… [+4280 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"nbc-news\",\n" +
            "                \"name\": \"NBC News\"\n" +
            "            },\n" +
            "            \"author\": \"Reuters\",\n" +
            "            \"title\": \"Saudi Arabia rallies around exiled Yemen leader after UAE-backed separatists seize Aden - NBCNews.com\",\n" +
            "            \"description\": \"Saudi Arabia threw its weight behind Yemen's exiled president, as UAE-backed southern separatists took over the port of Aden.\",\n" +
            "            \"url\": \"https://www.nbcnews.com/news/world/saudi-rallies-around-exiled-yemen-leader-after-uae-backed-separatists-n1041321\",\n" +
            "            \"urlToImage\": \"https://media3.s-nbcnews.com/j/newscms/2019_33/2967971/190812-aden-yemen-mc-1336_42b6bcccf25d8a4fccc15fbe2be5dfaa.nbcnews-fp-1200-630.JPG\",\n" +
            "            \"publishedAt\": \"2019-08-12T13:30:00Z\",\n" +
            "            \"content\": \"ADEN Saudi Arabia threw its weight behind Yemen's exiled president, as UAE-backed southern separatists who took over the port of Aden held strong against Riyadh's calls to vacate government sites.\\r\\nAn alliance of Sunni Arab states led by Riyadh has fractured … [+3674 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"9to5mac.com\"\n" +
            "            },\n" +
            "            \"author\": null,\n" +
            "            \"title\": \"Huge growth in Apple’s wearables mean they will overtake both iPad and Mac - 9to5Mac\",\n" +
            "            \"description\": \"Dramatic growth in Apple's wearables means that the product category – mostly comprising the Apple Watch and AirPods – will overtake each of iPad and Mac ...\",\n" +
            "            \"url\": \"https://9to5mac.com/2019/08/12/growth-in-apples-wearables/\",\n" +
            "            \"urlToImage\": \"https://9to5mac.com/wp-content/uploads/sites/6/2019/08/Dramatic-growth-in-Apples-wearables.jpg?quality=82&strip=all\",\n" +
            "            \"publishedAt\": \"2019-08-12T12:50:00Z\",\n" +
            "            \"content\": \"Dramatic growth in Apple’s wearables means that the product category mostly comprising the Apple Watch and AirPods  will overtake both iPad and Mac by the end of next year, says a new analysis.\\r\\nWhile the market is focused on growth in the Cupertino company’s… [+2620 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Aol.com\"\n" +
            "            },\n" +
            "            \"author\": \"Kamini Ramdeen\",\n" +
            "            \"title\": \"Man hailed as hero for thwarting gunman in Norway mosque shooting - AOL\",\n" +
            "            \"description\": \"A gunman who opened fire at a mosque outside Oslo, Norway was thwarted by a 65-year-old congregant who put him in a chokehold, authorities said Sunday.\",\n" +
            "            \"url\": \"https://www.aol.com/article/news/2019/08/12/man-hailed-as-hero-for-thwarting-gunman-in-norway-mosque-shooting-police-investigate-as-terror-attack/23791853/\",\n" +
            "            \"urlToImage\": \"https://o.aolcdn.com/images/dims3/GLOB/crop/449x295+0+42/resize/1028x675!/format/jpg/quality/85/https%3A%2F%2Fmedia.zenfs.com%2Fen-US%2Freuters.com%2F32a5bca60343a314bb3489be9e6473f7\",\n" +
            "            \"publishedAt\": \"2019-08-12T12:42:05Z\",\n" +
            "            \"content\": \"A gunman who opened fire at a mosque outside Oslo, Norway was thwarted by a 65-year-old congregant who put him in a chokehold, authorities said Sunday.\\r\\nThe shooting happened Saturday at the al-Noor Islamic Centre near the Norwegian capital, when the suspect,… [+8046 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"cbs-news\",\n" +
            "                \"name\": \"CBS News\"\n" +
            "            },\n" +
            "            \"author\": \"Kate Smith\",\n" +
            "            \"title\": \"Tennessee abortion laws: Tennessee to push for total abortion ban with sights on Supreme Court - CBS News\",\n" +
            "            \"description\": \"After struggling to pass a six-week abortion ban earlier this year, Tennessee lawmakers are considering one of the most restrictive abortion laws in the country: a total ban on the procedure\",\n" +
            "            \"url\": \"https://www.cbsnews.com/news/tennessee-abortion-laws-tennessee-to-push-for-total-abortion-ban-with-sights-on-supreme-court/\",\n" +
            "            \"urlToImage\": \"https://cbsnews3.cbsistatic.com/hub/i/r/2018/03/28/696253f4-7900-470d-84ec-5b348dbff142/thumbnail/1200x630/b680b12b9e56c55d6e3a986fd010e408/ap-090919060005.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T12:39:00Z\",\n" +
            "            \"content\": \"Nashville — After struggling to pass a six-week abortion ban earlier this year, Tennessee lawmakers are now considering one of the most restrictive abortion laws in the country: a total ban on the procedure.\\r\\nOn Monday and Tuesday, the state's judiciary commi… [+4678 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"rt\",\n" +
            "                \"name\": \"RT\"\n" +
            "            },\n" +
            "            \"author\": \"RT\",\n" +
            "            \"title\": \"Dow set for triple-digit losses, S&P and Nasdaq also down amid US-China trade tensions - RT\",\n" +
            "            \"description\": \"US stock index futures plunged on Monday, with the Dow Jones Industrial Average expected to open about  200 points down. The S&P 500 and Nasdaq futures are also pointing lower before the opening bell.\",\n" +
            "            \"url\": \"https://www.rt.com/business/466319-dow-futures-china-us/\",\n" +
            "            \"urlToImage\": \"https://cdni-rt.secure2.footprint.net/files/2019.08/article/5d514d7bdda4c8395e8b462a.JPG\",\n" +
            "            \"publishedAt\": \"2019-08-12T12:32:00Z\",\n" +
            "            \"content\": \"Losses sped up in pre-market trading after Hong Kong International Airport canceled all departures for the remainder of Monday after protesters flooded the air hub.\\r\\nAlso on rt.comSigns of terrorism: Beijing warns Hong Kong rioters, while airport grounds all … [+1553 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Waitingfornextyear.com\"\n" +
            "            },\n" +
            "            \"author\": null,\n" +
            "            \"title\": \"That other team up North: While We're Waiting - Waiting For Next Year\",\n" +
            "            \"description\": \"Impossible to call the Minnesota Twins and Cleveland Indians a rivalry. The ballclubs are teams that belong to the same division through happenstance. Is that changing?\",\n" +
            "            \"url\": \"https://waitingfornextyear.com/2019/08/cleveland-indians-minnesota-twins-rivalry/\",\n" +
            "            \"urlToImage\": \"https://waitingfornextyear.com/wp-content/uploads/2019/08/league-of-their-own-1024x576.jpeg\",\n" +
            "            \"publishedAt\": \"2019-08-12T12:30:31Z\",\n" +
            "            \"content\": \"A League of Their Own would have been a nice enough movie as an ongoing battle between the Racine Belles and Rockford Peaches for supremacy of the first women’s professional baseball league. However, the sibling rivalry of Kit Keller and Dottie Hinson defined… [+6031 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"fox-news\",\n" +
            "                \"name\": \"Fox News\"\n" +
            "            },\n" +
            "            \"author\": \"Madeline Farber\",\n" +
            "            \"title\": \"Massachusetts man diagnosed with deadly mosquito-borne virus; risk raised to 'critical' in nearby areas - Fox News\",\n" +
            "            \"description\": \"The first case of the potentially deadly Eastern Equine Encephalitis (EEE) virus has been confirmed in a Massachusetts man, the state’s department of public health announced over the weekend.\",\n" +
            "            \"url\": \"https://www.foxnews.com/health/massachusetts-mosquito-virus-eee\",\n" +
            "            \"urlToImage\": \"https://media2.foxnews.com/BrightCove/694940094001/2018/07/06/694940094001_5806301767001_5806320518001-vs.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T12:29:55Z\",\n" +
            "            \"content\": \"The first case of the potentially deadly Eastern Equine Encephalitis (EEE) virus has been confirmed in a Massachusetts man, the state’s department of public health announced over the weekend.\\r\\nThe man, who was not identified, is over 60 years old and lives in… [+2554 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Pagesix.com\"\n" +
            "            },\n" +
            "            \"author\": \"Francesca Bacardi\",\n" +
            "            \"title\": \"Liam Hemsworth breaks silence on Miley Cyrus breakup - Page Six\",\n" +
            "            \"description\": \"“You don’t understand what it’s like,” he said.\",\n" +
            "            \"url\": \"https://pagesix.com/2019/08/12/liam-hemsworth-breaks-silence-on-miley-cyrus-breakup/\",\n" +
            "            \"urlToImage\": \"https://nyppagesix.files.wordpress.com/2018/12/miley-cyrus-liam-hemsworth-1.jpg?quality=90&strip=all&w=1200\",\n" +
            "            \"publishedAt\": \"2019-08-12T12:27:00Z\",\n" +
            "            \"content\": \"Liam Hemsworth is mourning the end of his marriage to Miley Cyrus.\\r\\nThe 29-year-old actor has been spending time with brother Chris Hemsworth in Australia as Miley Cyrus has been flaunting a new romance with Brody Jenner‘s ex, Kaitlynn Carter, but he doesn’t … [+714 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"cbs-news\",\n" +
            "                \"name\": \"CBS News\"\n" +
            "            },\n" +
            "            \"author\": \"CBS News\",\n" +
            "            \"title\": \"Jeffrey Epstein death in prison shifts focus to Ghislaine Maxwell, British alleged co-conspirator and acquaintance of Prince Andrew - CBS News\",\n" +
            "            \"description\": \"Socialite Ghislaine Maxwell has been accused of finding teenage girls for Epstein and his friends, including a British royal\",\n" +
            "            \"url\": \"https://www.cbsnews.com/news/epstein-death-britain-alleged-co-conspirator-ghislaine-maxwell-prince-andrew-2019-08-12/\",\n" +
            "            \"urlToImage\": \"https://cbsnews3.cbsistatic.com/hub/i/r/2019/08/12/4118150d-2192-4c19-a38b-ccb05dff597a/thumbnail/1200x630/f8cd056dbc534ab09090388330751def/prince-andrew-virginia-giuffre-ghislaine-maxwell.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T11:40:00Z\",\n" +
            "            \"content\": \"London -- The death of Jeffrey Epstein is putting new attention on his alleged co-conspirators, who could still face charges. The number one person on that list is Ghislaine Maxwell, who's accused of finding teenage girls for Epstein and his friends -- includ… [+2703 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"the-washington-post\",\n" +
            "                \"name\": \"The Washington Post\"\n" +
            "            },\n" +
            "            \"author\": \"Spencer S. Hsu\",\n" +
            "            \"title\": \"Trial of former Obama White House counsel Gregory Craig highlights crackdown on foreign-influence industry - Washington Post\",\n" +
            "            \"description\": \"Craig, 74, has pleaded not guilty in case set to start Monday in federal court in Washington.\",\n" +
            "            \"url\": \"https://www.washingtonpost.com/local/legal-issues/trial-of-former-obama-white-house-counsel-gregory-craig-highlights-crackdown-on-foreign-influence-industry/2019/08/11/7d61aadc-b797-11e9-bad6-609f75bfd97f_story.html\",\n" +
            "            \"urlToImage\": \"https://www.washingtonpost.com/resizer/BAEBpjSvLg8bxLDGyCGVSEcqckI=/1484x0/arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/RKAUY5S5HEI6TBBNPU7NP2ZZK4.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T11:00:08Z\",\n" +
            "            \"content\": \"Former Obama White House counsel Gregory B. Craig faces trial Monday for allegedly lying to the Justice Department in a prosecution that has shaken up the capitals billion-dollar foreign influence industry.\\r\\nIn charging Craig one of Washingtons most prominent… [+8794 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"the-new-york-times\",\n" +
            "                \"name\": \"The New York Times\"\n" +
            "            },\n" +
            "            \"author\": null,\n" +
            "            \"title\": \"Is Your Sleep Cycle Out of Sync? It May Be Genetic - The New York Times\",\n" +
            "            \"description\": \"Sleep patterns often run in families, and researchers have been identifying genes that influence them.\",\n" +
            "            \"url\": \"https://www.nytimes.com/2019/08/12/well/mind/is-your-sleep-cycle-out-of-sync-it-may-be-genetic.html\",\n" +
            "            \"urlToImage\": \"https://static01.nyt.com/images/2019/08/13/science/13BRODYSLEEP/13BRODYSLEEP-facebookJumbo.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T09:00:10Z\",\n" +
            "            \"content\": \"Chances are, too, there are far more extreme larks than come to professional attention. The team pointed out that people with advanced sleep phase rarely consult sleep doctors or are studied in sleep clinics because most of those affected seem to like the pat… [+2653 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"nbc-news\",\n" +
            "                \"name\": \"NBC News\"\n" +
            "            },\n" +
            "            \"author\": \"Mary Pflum\",\n" +
            "            \"title\": \"Nation's fertility clinics struggle with a growing number of abandoned embryos - NBCNews.com\",\n" +
            "            \"description\": \"Fertility doctors throughout the country grappling with what to do with the embryos cast aside by former patients.\",\n" +
            "            \"url\": \"https://www.nbcnews.com/health/features/nation-s-fertility-clinics-struggle-growing-number-abandoned-embryos-n1040806\",\n" +
            "            \"urlToImage\": \"https://media2.s-nbcnews.com/j/newscms/2019_27/2920616/190703-july-egg-freezing-followup-main-kh_28db9baeadfe83ae139ff549e3f084a7.nbcnews-fp-1200-630.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T08:34:00Z\",\n" +
            "            \"content\": \"Patrizio said storage fees at his clinic cost \$600 a year, but can cost twice as much, depending upon the clinic.\\r\\nThe problem is, even if an embryo is considered abandoned, even if theres a contract in place, its very difficult to get rid of. What if one day… [+8500 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Express.co.uk\"\n" +
            "            },\n" +
            "            \"author\": \"Sebastian Kettley\",\n" +
            "            \"title\": \"Perseids NASA live stream: How to watch the Perseid meteor shower online - Express.co.uk\",\n" +
            "            \"description\": \"THE Perseid meteor shower peaks tonight and NASA’s meteor-hunters are excited to broadcast the dazzling shower for the world to see. Find out how to watch the Perseid meteor shower live online with NASA tonight.\",\n" +
            "            \"url\": \"https://www.express.co.uk/news/science/1163948/Perseids-NASA-live-stream-how-to-watch-Perseids-meteor-shower-online-livestream\",\n" +
            "            \"urlToImage\": \"https://cdn.images.express.co.uk/img/dynamic/151/750x445/1163948.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T07:58:00Z\",\n" +
            "            \"content\": null\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"reuters\",\n" +
            "                \"name\": \"Reuters\"\n" +
            "            },\n" +
            "            \"author\": \"Pei Li\",\n" +
            "            \"title\": \"Coach, Givenchy in hot water over China T-shirt row - Reuters\",\n" +
            "            \"description\": \"Chinese brand ambassadors of fashion labels from Coach to Givenchy have severed ties with the companies over products which they said violated China's sovereignty by identifying Hong Kong and Taiwan as countries.\",\n" +
            "            \"url\": \"https://www.reuters.com/article/us-china-brands-politics/coach-givenchy-in-hot-water-over-china-t-shirt-row-idUSKCN1V20GM\",\n" +
            "            \"urlToImage\": \"https://s2.reutersmedia.net/resources/r/?m=02&d=20190812&t=2&i=1418086337&w=1200&r=LYNXNPEF7B0DB\",\n" +
            "            \"publishedAt\": \"2019-08-12T06:49:00Z\",\n" +
            "            \"content\": \"BEIJING/HONG KONG (Reuters) - Chinese brand ambassadors of fashion labels from Coach to Givenchy have severed ties with the companies over products which they said violated China’s sovereignty by identifying Hong Kong and Taiwan as countries. \\r\\nThe brands are… [+3186 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"reuters\",\n" +
            "                \"name\": \"Reuters\"\n" +
            "            },\n" +
            "            \"author\": \"Rania El Gamal\",\n" +
            "            \"title\": \"Saudi Aramco aims to buy Reliance stake, reports lower earnings - Reuters\",\n" +
            "            \"description\": \"Saudi Aramco is planning a multibillion dollar investment in India's Reliance Industries as the energy giant diversifies its oil business, where weaker prices cut its first-half profit by 12%.\",\n" +
            "            \"url\": \"https://www.reuters.com/article/us-saudi-aramco-results/saudi-aramco-to-buy-reliance-stake-reports-12-earnings-slide-idUSKCN1V20FA\",\n" +
            "            \"urlToImage\": \"https://s2.reutersmedia.net/resources/r/?m=02&d=20190812&t=2&i=1418058382&w=1200&r=LYNXNPEF7B0CC\",\n" +
            "            \"publishedAt\": \"2019-08-12T06:24:00Z\",\n" +
            "            \"content\": \"DUBAI/MUMBAI (Reuters) - Saudi Aramco is planning a multibillion dollar investment in India’s Reliance Industries (RELI.NS) as the energy giant diversifies its oil business, where weaker prices cut its first-half profit by 12%. \\r\\nIn preparation for what could… [+5627 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Youtube.com\"\n" +
            "            },\n" +
            "            \"author\": null,\n" +
            "            \"title\": \"India-administered Kashmir remains cut off during Eid Al-Adha - Al Jazeera English\",\n" +
            "            \"description\": \"Security remains tight in Indian-administered Kashmir, although some restrictions have been eased to allow people to celebrate the Islamic holiday Eid Al-Adh...\",\n" +
            "            \"url\": \"https://www.youtube.com/watch?v=w78k6DIUzPU\",\n" +
            "            \"urlToImage\": \"https://i.ytimg.com/vi/w78k6DIUzPU/maxresdefault.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T05:32:25Z\",\n" +
            "            \"content\": \"Security remains tight in Indian-administered Kashmir, although some restrictions have been eased to allow people to celebrate the Islamic holiday Eid Al-Adha. Telephone and internet connections are still cut off after the Indian government revoked the autono… [+517 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"the-hill\",\n" +
            "                \"name\": \"The Hill\"\n" +
            "            },\n" +
            "            \"author\": \"Chris Mills Rodrigo\",\n" +
            "            \"title\": \"Scaramucci: GOP may need to replace Trump for 2020 | TheHill - The Hill\",\n" +
            "            \"description\": \"Former White House communications director ...\",\n" +
            "            \"url\": \"https://thehill.com/homenews/administration/457050-scaramucci-gop-may-need-to-replace-trump-for-2020\",\n" +
            "            \"urlToImage\": \"https://thehill.com/sites/default/files/scaramuccianthony10022017lorenz.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-12T00:32:59Z\",\n" +
            "            \"content\": \"Former White House communications director Anthony ScaramucciAnthony ScaramucciScaramucci fires back at Trump: He will turn 'on everyone' and then 'entire country' Trump blasts Scaramucci as 'incapable'Bill Maher roots for recession so that Trump loses in 202… [+2564 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Phonearena.com\"\n" +
            "            },\n" +
            "            \"author\": \"Joshua Swingle\",\n" +
            "            \"title\": \"Possible Google Pixel 4 camera sample hints at incredible new feature - PhoneArena\",\n" +
            "            \"description\": \"The Google Pixel 4 could support up to 20x zoom via a special DSLR-like camera attachment, a possible camera sample suggest. The smartphone will also include a dual-camera setup on the rear ...\",\n" +
            "            \"url\": \"https://www.phonearena.com/news/Google-Pixel-4-camera-sample-zoom-feature_id118130\",\n" +
            "            \"urlToImage\": \"https://i-cdn.phonearena.com/images/article/118130-two_lead/Possible-Google-Pixel-4-camera-sample-hints-at-incredible-new-feature.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-11T20:58:03Z\",\n" +
            "            \"content\": \"All content, features, and design are Copyright 2001-2019 PhoneArena.com. All rights reserved. Reproduction in whole or in part or in any form or medium without written permission is prohibited!\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": \"usa-today\",\n" +
            "                \"name\": \"USA Today\"\n" +
            "            },\n" +
            "            \"author\": \"Henry McKenna\",\n" +
            "            \"title\": \"Brittney Griner among 6 ejected from WNBA game after throwing punches in fight - For The Win\",\n" +
            "            \"description\": \"Brittney Griner threw punches and chased Kristine Anigwe down the court.\",\n" +
            "            \"url\": \"https://ftw.usatoday.com/2019/08/brittney-griner-ejected-fighting-punches-wnba\",\n" +
            "            \"urlToImage\": \"https://usatftw.files.wordpress.com/2019/08/gettyimages-1160877122-e1565538470735.jpg?w=1024&h=576&crop=1\",\n" +
            "            \"publishedAt\": \"2019-08-11T15:55:00Z\",\n" +
            "            \"content\": \"Brittney Griner was at the center of a fight during the Dallas Wings’ 80-77 win over the Phoenix Mercury at Talking Stick Resort Arena on Saturday.\\r\\nThe Mercury center threw punches at Dallas rookie Kristine Anigwe, who resorted to running from Griner. Though… [+1260 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Notebookcheck.net\"\n" +
            "            },\n" +
            "            \"author\": \"Ricci Rox\",\n" +
            "            \"title\": \"Samsung's new devices are a poor joke and the base Note 10 is the punchline - Notebookcheck.net\",\n" +
            "            \"description\": \"Samsung launched the Galaxy Note 10 phones a few days ago and the devices have received mixed reviews, most notably for the lack of a headphone jack and the...well, the general existence of the Note 10.\",\n" +
            "            \"url\": \"https://www.notebookcheck.net/Samsung-s-new-devices-are-a-poor-joke-and-the-base-Note-10-is-the-punchline.429955.0.html\",\n" +
            "            \"urlToImage\": \"https://www.notebookcheck.net/fileadmin/Notebooks/News/_nc3/note_10_yahoo_3.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-11T13:55:04Z\",\n" +
            "            \"content\": \"Four days ago, Samsung finally launched the Galaxy Note 10 phones. For the first time, the company released two Note devices, much in the same way it launched three Galaxy S10 phones earlier in the year. Sadly, while the S10 trio has received positive reviews… [+4200 chars]\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"source\": {\n" +
            "                \"id\": null,\n" +
            "                \"name\": \"Bigislandvideonews.com\"\n" +
            "            },\n" +
            "            \"author\": null,\n" +
            "            \"title\": \"VIDEO: TMT Opponents Refute Observatories After Announcement - Big Island Video News\",\n" +
            "            \"description\": \"MAUNAKEA, Hawaiʻi - During a news conference held on the Mauna Kea Access Road, Lanakila Mangauil responded to Friday's news that observatories would be returning to work at the summit.\",\n" +
            "            \"url\": \"https://www.bigislandvideonews.com/2019/08/10/video-tmt-opponents-refute-observatories-after-announcement/\",\n" +
            "            \"urlToImage\": \"https://www.bigislandvideonews.com/wp-content/uploads/2019/08/2019-08-10-mangauil-2.jpg\",\n" +
            "            \"publishedAt\": \"2019-08-11T00:52:22Z\",\n" +
            "            \"content\": \"(BIVN) – On Friday, the existing Mauna Kea observatories announced that they will return to work after a four-week suspension.\\r\\nOn July 16, a day after the standoff against the construction of the planned Thirty Meter Telescope began, the observatories made t… [+2465 chars]\"\n" +
            "        }\n" +
            "    ]\n" +
            "}"

}
