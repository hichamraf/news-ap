package hicham.com.bonialnews.home

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import hicham.com.bonialnews.data.local.LocalDB
import hicham.com.bonialnews.data.local.NewsDao
import hicham.com.bonialnews.model.Article
import hicham.com.bonialnews.model.Source
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class RoomDaoTesting {
    private lateinit var newsDao: NewsDao
    private lateinit var db: LocalDB
    private lateinit var article: Article

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().getTargetContext()
        db = Room.inMemoryDatabaseBuilder(
            context, LocalDB::class.java).build()
        newsDao = db.newsDao()
        article= Article(1,"hicham","headline news","news desc","url","utl","content","09-09-2190", Source("1","morocco"))
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeUserAndReadInList() {
        newsDao.insertArticles(listOf(article))
        val articlefromDb = newsDao.getArticles(0)[0]
        Assert.assertEquals(article.author, articlefromDb.author)
    }
}