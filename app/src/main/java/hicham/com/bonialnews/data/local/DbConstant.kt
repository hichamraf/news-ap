package hicham.com.bonialnews.data.local

class DbConstant {
    companion object {
        const val DB_NAME = "news_database"
        const val ARTICLE_ID="article_id"
        const val NEWS_TABLE_NAME = "Articles"
        const val ARTICLE_AUTHOR = "author"
        const val ARTICLE_TITLE = "title"
        const val ARTICLE_DESC = "description"
        const val ARTICLE_URL = "url"
        const val ARTICLE_URLTOIMAGE = "urlToImage"
        const val ARTICLE_PUBLISHEDAT = "publishedAt"
        const val ARTICLE_CONTENT = "content"
        const val ARTICLE_SOURCE="source"
        const val SOURCE_ID = "id"
        const val SOURCE_NAME = "name"
    }
}