package hicham.com.bonialnews.data

import hicham.com.bonialnews.Utils.ConnectivityUtil
import hicham.com.bonialnews.data.local.articles.ArticlesLocalApi
import hicham.com.bonialnews.data.remote.articles.ArticlesRemoteApi
import hicham.com.bonialnews.model.Article
import hicham.com.bonialnews.model.NewsApiResponse
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class ArticleRepository(
    private var articlesLocalApi: ArticlesLocalApi,
    private var articlesRemoteApi: ArticlesRemoteApi,
    var connectivityUtil: ConnectivityUtil
) {

    var pageSize = 21

    fun getArticles(page: Int): Single<List<Article>> {
        if (connectivityUtil.haveNetworkConnection() && page == 0) {

            return  articlesRemoteApi.getArticles(page).subscribeOn(Schedulers.io())
                .map { response: NewsApiResponse -> response.articles }
                .doOnSuccess { articles: List<Article> ->
                    articlesLocalApi.deleteAll()
                    articlesLocalApi.addArticles(
                        articles
                    )
                }
                .map { it.take(pageSize) }
                .onErrorResumeNext(
                    articlesLocalApi.getArticles(
                        page * pageSize
                    )
                )


        } else {
            return articlesLocalApi.getArticles(page * pageSize).subscribeOn(Schedulers.io())
        }


    }
}