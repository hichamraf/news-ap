package hicham.com.bonialnews.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hicham.com.bonialnews.model.Article

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticles(articles: List<Article>)

    @Query("select * from " + DbConstant.NEWS_TABLE_NAME + " LIMIT " + 21 + " OFFSET :offset")
    fun getArticles(offset: Int): List<Article>

    @Query("DELETE FROM " + DbConstant.NEWS_TABLE_NAME)
    fun deleteAll()
}