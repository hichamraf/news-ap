package hicham.com.bonialnews.data.local.articles

import androidx.annotation.WorkerThread
import hicham.com.bonialnews.data.local.NewsDao
import hicham.com.bonialnews.model.Article
import io.reactivex.Single
import javax.inject.Inject

class ArticlesLocalApiImpli @Inject constructor(var newsDao: NewsDao) : ArticlesLocalApi {
    override fun deleteAll() {
        newsDao.deleteAll()
    }

    override fun getArticles(offset: Int): Single<List<Article>> {
        return Single.fromCallable { newsDao.getArticles(offset) }
    }

    @WorkerThread
    override fun addArticles(articles: List<Article>) {
        newsDao.insertArticles(articles)
    }
}