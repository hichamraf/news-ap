package hicham.com.bonialnews.data.remote.articles

import hicham.com.bonialnews.model.NewsApiResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticlesRemoteApi {
    @GET("top-headlines")
    fun getArticles(@Query("page") page: Int): Single<NewsApiResponse>
}