package hicham.com.bonialnews.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import hicham.com.bonialnews.model.Article

@Database(entities = [Article::class], version = 1, exportSchema = false)
abstract class LocalDB : RoomDatabase() {
    abstract fun newsDao(): NewsDao
}