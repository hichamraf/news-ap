package hicham.com.bonialnews.data.local.articles

import hicham.com.bonialnews.model.Article
import io.reactivex.Single

interface ArticlesLocalApi {

    fun getArticles(offset: Int): Single<List<Article>>

    fun addArticles(articles: List<Article>)

    fun deleteAll()
}