package hicham.com.bonialnews.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import hicham.com.bonialnews.data.local.DbConstant

data class Source(@Expose @SerializedName(DbConstant.SOURCE_ID)  @ColumnInfo(name = DbConstant.SOURCE_ID) var id: String?, @Expose @SerializedName(DbConstant.SOURCE_NAME) @ColumnInfo(name=DbConstant.SOURCE_NAME)  var name: String?)