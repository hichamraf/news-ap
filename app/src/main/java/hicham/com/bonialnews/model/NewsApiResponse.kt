package hicham.com.bonialnews.model

import com.google.gson.annotations.SerializedName

data class NewsApiResponse(@SerializedName("articles") var articles : List<Article>)