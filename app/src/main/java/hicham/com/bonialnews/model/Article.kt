package hicham.com.bonialnews.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import hicham.com.bonialnews.data.local.DbConstant

@Entity(tableName = DbConstant.NEWS_TABLE_NAME)
data class Article(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = DbConstant.ARTICLE_ID) var id : Int,
    @Expose @ColumnInfo(name = DbConstant.ARTICLE_AUTHOR) @SerializedName(DbConstant.ARTICLE_AUTHOR) var author: String?, @Expose @ColumnInfo(
        name = DbConstant.ARTICLE_TITLE
    ) @SerializedName(DbConstant.ARTICLE_TITLE) var title: String
    , @Expose @ColumnInfo(name = DbConstant.ARTICLE_DESC) @SerializedName(DbConstant.ARTICLE_DESC) var description: String?, @Expose @ColumnInfo(
        name = DbConstant.ARTICLE_URL
    ) @SerializedName(DbConstant.ARTICLE_URL) var url : String ?, @Expose @ColumnInfo(name = DbConstant.ARTICLE_URLTOIMAGE) @SerializedName(
        DbConstant.ARTICLE_URLTOIMAGE
    ) var urlToImage: String?, @Expose @ColumnInfo(name = DbConstant.ARTICLE_CONTENT) @SerializedName(DbConstant.ARTICLE_CONTENT) var content: String ?
    , @Expose @ColumnInfo(name = DbConstant.ARTICLE_PUBLISHEDAT) @SerializedName(DbConstant.ARTICLE_PUBLISHEDAT) var publishedAt: String, @Expose @Embedded @SerializedName(
        DbConstant.ARTICLE_SOURCE
    ) var source: Source
)