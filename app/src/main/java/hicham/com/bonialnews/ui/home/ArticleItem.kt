package hicham.com.bonialnews.ui.home

import hicham.com.bonialnews.model.Article
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


data class ArticleItem(var article: Article) {


    // from stackOverflow https://stackoverflow.com/questions/2003521/find-total-hours-between-two-dates/2003612
    fun getFormattedPublicationDate(): Int {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        var articleDate: Date? = null
        try {
            articleDate = dateFormat.parse(article.publishedAt)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val calendar = Calendar.getInstance()
        calendar.time = articleDate
        var cuurentTime = Calendar.getInstance().time
        val timeDiffinSecs = (cuurentTime.time - articleDate!!.getTime()) / 1000
        return (timeDiffinSecs / 3600).toInt()

    }
}