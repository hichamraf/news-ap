package hicham.com.bonialnews.ui.home

import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import dagger.android.support.DaggerAppCompatActivity
import hicham.com.bonialnews.R
import hicham.com.bonialnews.databinding.ActivityMainBinding
import hicham.com.bonialnews.di.viewmodel.ViewModelFactory
import hicham.com.bonialnews.model.Article
import hicham.com.bonialnews.ui.home.MainViewModel.RequestState.LOADING
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity() {
    private lateinit var viewModel: MainViewModel
    @Inject
    lateinit var factory: ViewModelFactory

    @Inject
    lateinit var requestManager: RequestManager

    private lateinit var articles: List<Article>
    private lateinit var binding: ActivityMainBinding
    private lateinit var articlesAdapter: ArticlesAdapter
    private var cd = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)
        articlesAdapter = ArticlesAdapter(requestManager) {
            showDialog(it)
        }
        cd.add(viewModel.articlesObservale.subscribe { articles: List<ArticleItem> ->
            articlesAdapter.updateArticles(articles)
        })

        cd.add(viewModel.requestsObservable.subscribe {
            when (it) {
                LOADING -> binding.requestStatePb.visibility = View.VISIBLE
                else -> binding.requestStatePb.visibility = View.INVISIBLE
            }
        })


        val portraitGridLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        portraitGridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position % 7 == 0) 2 else 1
            }
        }
        val lanscapeGridLayoutManager = GridLayoutManager(this, 3, RecyclerView.VERTICAL, false)
        lanscapeGridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position % 7 == 0) 3 else 1
            }
        }
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            binding.articlesRecyclerView.layoutManager = portraitGridLayoutManager
        }
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            binding.articlesRecyclerView.layoutManager = lanscapeGridLayoutManager
        }

        binding.articlesRecyclerView.adapter = articlesAdapter

        val layoutManager = binding.articlesRecyclerView.layoutManager as GridLayoutManager
        binding.articlesRecyclerView.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

                viewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount)
            }
        })
    }

    internal fun showDialog(article: ArticleItem) {
        val webDialogFragment = WebDialogFragment.newInstance(article.article.url!!, article.article.title)
        webDialogFragment.show(supportFragmentManager, WebDialogFragment.TAG)
    }

    override fun onStop() {
        cd.clear()
        super.onStop()
    }

    override fun onDestroy() {
        cd.dispose()
        super.onDestroy()
    }
}
