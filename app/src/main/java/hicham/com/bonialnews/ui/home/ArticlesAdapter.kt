package hicham.com.bonialnews.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import hicham.com.bonialnews.R
import hicham.com.bonialnews.databinding.ArticleItemBinding
import javax.inject.Inject

class ArticlesAdapter(private var requestManager: RequestManager, val callback: (ArticleItem) -> Unit) :
    RecyclerView.Adapter<ArticlesAdapter.ArticleViewHolder>() {
    var articles: ArrayList<ArticleItem> = ArrayList()
    @Inject
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val binding = DataBindingUtil.inflate<ArticleItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.article_item,
            parent,
            false
        )
        return ArticleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
       holder.binding.ai = articles[position]
        requestManager.load(articles[position].article.urlToImage)
            .into(holder.binding.articleImage)
        if (position % 7 ==0)
            holder.binding.articleContentTxt.text=articles[position].article.content
        else
            holder.binding.articleContentTxt.text=articles[position].article.description
        holder.binding.newsLayout.setOnClickListener {
            callback(articles[position])
        }
    }

    fun updateArticles(articles: List<ArticleItem>) {
        var lastCount = this.articles.size
        this.articles.addAll(ArrayList(articles))
        notifyItemRangeInserted(lastCount, articles.size)
    }

    override fun getItemCount(): Int {
        return articles.size
    }


    class ArticleViewHolder(var binding: ArticleItemBinding) : RecyclerView.ViewHolder(binding.root)
}