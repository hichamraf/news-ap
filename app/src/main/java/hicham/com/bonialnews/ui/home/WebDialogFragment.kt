package hicham.com.bonialnews.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.DialogFragment
import hicham.com.bonialnews.R
import hicham.com.bonialnews.databinding.FragmentWebDialogBinding

class WebDialogFragment : DialogFragment() {
    private var binding: FragmentWebDialogBinding? = null
    private val isLoading = ObservableBoolean(true)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.ModalWebViewStyle)
    }

    override fun onStart() {
        super.onStart()
        val window = dialog!!.window
        window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_web_dialog, container, false)
        binding!!.isLoading = isLoading
        binding!!.url.text = arguments!!.getString(URL)
        binding!!.header.text = arguments!!.getString(HEADER)
        binding!!.webview.settings.javaScriptEnabled = true
        binding!!.webview.loadUrl(arguments!!.getString(URL))
        binding!!.webview.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                isLoading.set(false)
            }
        }
        return binding!!.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding!!.exit.setOnClickListener { v -> dismiss() }
    }

    companion object {

        val TAG = WebDialogFragment::class.java.simpleName

        val HEADER = "header"
        val URL = "url"

        fun newInstance(url: String, header: String): WebDialogFragment {
            val adf = WebDialogFragment()
            val bundle = Bundle(2)
            bundle.putString(HEADER, header)
            bundle.putString(URL, url)
            adf.arguments = bundle
            return adf
        }
    }
}