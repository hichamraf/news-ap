package hicham.com.bonialnews.ui.home

import androidx.lifecycle.ViewModel
import hicham.com.bonialnews.data.ArticleRepository
import hicham.com.bonialnews.model.Article
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class MainViewModel @Inject constructor(var articleRepository: ArticleRepository) : ViewModel() {
    var cd: CompositeDisposable
    var articlesObservale: BehaviorSubject<List<ArticleItem>> = BehaviorSubject.create()
    var requestsObservable: BehaviorSubject<RequestState> = BehaviorSubject.create()
    var lastArticlLoaded = false
    private var isLoadig = false
    var lastRequestedPage = 0


    companion object {
        private const val VISIBLE_THRESHOLD = 5
    }

    init {
        cd = CompositeDisposable()
        loadArticles()
    }


    fun loadArticles() {
        cd.add(articleRepository.getArticles(lastRequestedPage).observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                requestsObservable.onNext(RequestState.LOADING)
                isLoadig = true
            }
            .doOnError { requestsObservable.onNext(RequestState.ERROR) }
            .map { it.map { ArticleItem(it) } }
            .doOnSuccess {
                requestsObservable.onNext(
                    RequestState.COMPLETE
                )
                articlesObservale.onNext(it)
                if (it.size < 21) {
                    lastArticlLoaded = true
                }
                isLoadig = false
            }.subscribe()
        )
    }

    enum class RequestState {
        LOADING, SUCCESS, ERROR, COMPLETE
    }

    override fun onCleared() {
        cd.clear()
        super.onCleared()
    }


    fun listScrolled(visibleItemCount: Int, lastVisibleItemPosition: Int, totalItemCount: Int) {
        if (visibleItemCount + lastVisibleItemPosition + VISIBLE_THRESHOLD >= totalItemCount && !lastArticlLoaded && !isLoadig) {
            lastRequestedPage++
            loadArticles()
        }
    }


}