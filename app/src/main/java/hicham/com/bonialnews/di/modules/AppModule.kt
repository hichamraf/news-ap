package hicham.com.bonialnews.di.modules

import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import hicham.com.bonialnews.BuildConfig
import hicham.com.bonialnews.R
import hicham.com.bonialnews.ui.BaseApplication
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return Gson()
    }

    @Singleton
    @Provides
    internal fun providesRequestManager(application: BaseApplication, requestOptions: RequestOptions): RequestManager {
        return Glide.with(application).applyDefaultRequestOptions(requestOptions)
    }

    @Singleton
    @Provides
    internal fun provideRequestManager(): RequestOptions {
        return RequestOptions
            .placeholderOf(R.drawable.place_holder)
            .error(R.drawable.fail_load)
    }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl("https://newsapi.org/v2/")
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val okHttpClient = OkHttpClient.Builder()
        okHttpClient.addInterceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()
            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("apiKey", BuildConfig.ApiKey)
                .addQueryParameter("pageSize", "100")
                .addQueryParameter("country", "us")
                .build()
            val builder = original.newBuilder().url(url)
            val request = builder.build()

            chain.proceed(request)
        }
        return okHttpClient.build()
    }

}