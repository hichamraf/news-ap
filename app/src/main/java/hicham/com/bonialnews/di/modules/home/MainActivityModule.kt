package hicham.com.bonialnews.di.modules.home

import dagger.Module
import dagger.android.ContributesAndroidInjector
import hicham.com.bonialnews.ui.home.MainActivity

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract fun provideMainActivity() : MainActivity
}