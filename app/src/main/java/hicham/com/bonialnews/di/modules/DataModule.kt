package hicham.com.bonialnews.di.modules

import androidx.room.Room
import dagger.Module
import dagger.Provides
import hicham.com.bonialnews.Utils.ConnectivityUtil
import hicham.com.bonialnews.data.ArticleRepository
import hicham.com.bonialnews.data.local.DbConstant
import hicham.com.bonialnews.data.local.LocalDB
import hicham.com.bonialnews.data.local.NewsDao
import hicham.com.bonialnews.data.local.articles.ArticlesLocalApi
import hicham.com.bonialnews.data.local.articles.ArticlesLocalApiImpli
import hicham.com.bonialnews.data.remote.articles.ArticlesRemoteApi
import hicham.com.bonialnews.ui.BaseApplication
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class DataModule {


    @Provides
    fun provideLocalDb(baseApplication: BaseApplication) : LocalDB{
        return Room.databaseBuilder(baseApplication,LocalDB::class.java,DbConstant.DB_NAME).build()
    }

    @Singleton
    @Provides
    fun provideArticleLocalApi(newsDao: NewsDao): ArticlesLocalApi {
        return ArticlesLocalApiImpli(newsDao)
    }

    @Singleton
    @Provides
    fun provideArticleRemoteApi(retrofit: Retrofit): ArticlesRemoteApi {
        return retrofit.create(ArticlesRemoteApi::class.java)
    }

    @Singleton
    @Provides
    fun provideArticleRepo(
        articlesLocalApi: ArticlesLocalApi,
        articlesRemoteApi: ArticlesRemoteApi, connectivityUtil: ConnectivityUtil
    ): ArticleRepository {
        return ArticleRepository(articlesLocalApi, articlesRemoteApi, connectivityUtil)
    }

    @Provides
    fun providenewsDao(localDB: LocalDB) : NewsDao{
        return localDB.newsDao()
    }

}