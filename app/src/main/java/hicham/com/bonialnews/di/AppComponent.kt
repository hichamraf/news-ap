package hicham.com.bonialnews.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import hicham.com.bonialnews.di.modules.AppModule
import hicham.com.bonialnews.di.modules.DataModule
import hicham.com.bonialnews.di.modules.ViewModelModule
import hicham.com.bonialnews.di.modules.home.MainActivityModule
import hicham.com.bonialnews.ui.BaseApplication
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, DataModule::class, AppModule::class, ViewModelModule::class, MainActivityModule::class])
interface AppComponent : AndroidInjector<BaseApplication> {


    @Component.Builder
    interface Builder{
        @BindsInstance
       fun application(baseApplication: BaseApplication) : Builder
        fun build() : AppComponent
    }

}