package hicham.com.bonialnews.Utils

import android.content.Context
import android.net.ConnectivityManager
import hicham.com.bonialnews.ui.BaseApplication
import javax.inject.Inject

class ConnectivityUtil @Inject constructor(private var baseApplication: BaseApplication) {

    fun haveNetworkConnection(): Boolean {
        val cm = baseApplication.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }
}